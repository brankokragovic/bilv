GIT
PHP >= 7.0,
OpenSSL PHP Extension,
PDO PHP Extension,
Mbstring PHP Extension,
Tokenizer PHP Extension,
BCMath PHP Extension (required when the Hash ID feature is enabled),
Composer,
Node (required for the API Docs generator feature),
Web Server (Nginx is recommended),
Database Engine (no recommendation),
Cache Engine (Redis is recommended),
Queues Engine (Beanstalkd is recommended) # Installation using Laravel Homestead,
git clone git@bitbucket.org:brankokragovic/bilv.git,
cd bilv/,
cp .env.example  .env,
composer install,
vendor/bin/homestead make,
edit Homestead.yml with your configuration,
Homestead.yml example:
##########################################
ip: 192.168.10.16
memory: 2048
cpus: 1
provider: virtualbox
authorize: ~/.ssh/id_rsa.pub
keys:
    - ~/.ssh/id_rsa
folders:
    -
        map: /home/branko/bilv
        to: /home/vagrant/code
sites:
    -
        map: api.bilv.dev
        to: /home/vagrant/code/public

        map: bilv.dev
        to: /home/vagrant/code/public
databases:
    - bilv
name: bilv
hostname: bilv
###########################################

vagrant up,
vagrant ssh,
cd code/,
php artisan migrate,
php artisan passport:install,
php artisan db:seed,
sudo npm install apidoc -g,
npm install apidoc,
php artisan apiato:docs,
php artisan storage:link,
php artisan key:generate,


############################################
edit your hosts file:

## bilv ##
192.168.10.16 bilv.dev api.api.dev